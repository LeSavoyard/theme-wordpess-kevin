<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Theme_de_Kevin
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="styles.css">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php get_custom_logo() ?><?php esc_html_e('Skip to content', 'kf-theme'); ?></a>

		<header id="masthead" class="site-header">
			<nav>
				<a href="<?php echo esc_url(home_url('/')); ?>"> Home</a>
				<a href="archive.php">Liste des articles</a>

			</nav>
			<?php
				if (is_single() || is_archive()){
					?>
					<img class="headerimageart" src="<?php the_post_thumbnail() ?>">
					<?php
				}
				else {
					?>
					<img class="headerimagehome" src="<?php header_image() ?>" alt="" srcset="">
					<?php
				}
			?>
			
			<a href="<?php echo esc_url(home_url('/')); ?>">
				<h1><?php the_custom_logo() ?><?php bloginfo('name') ?></h1>
			</a>
		</header><!-- #masthead -->

		<div id="content" class="site-content">