<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Theme_de_Kevin
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<section class="error-404 not-found">
			<img src="https://media.giphy.com/media/A9EcBzd6t8DZe/giphy.gif" id="404" alt="" srcset="">
			<p><?php _e('It looks like nothing was found at this location. Maybe try a search?', 'twentyseventeen'); ?></p>

			<?php get_search_form(); ?>
		</section><!-- .error-404 -->




	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
